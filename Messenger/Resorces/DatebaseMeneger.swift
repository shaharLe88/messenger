//
//  DatebaseMeneger.swift
//  Messenger
//
//  Created by admin on 24/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
import FirebaseDatabase

final class DatabaseMeneger{
    static let shared = DatabaseMeneger()
    
    private let database = Database.database().reference()
    
    static func safeEmail(emailAddress: String) -> String{
        var safeEmail = emailAddress.replacingOccurrences(of: ".", with: "-")
        safeEmail.replacingOccurrences(of: "@", with: "-")
        return safeEmail
    }
    
}

// Account Menagement
extension DatabaseMeneger {
    
    public func userExists(with email: String, complition: @escaping ((Bool) -> Void)){
        let safeEmail = email.replacingOccurrences(of: ".", with: "-")
        safeEmail.replacingOccurrences(of: "@", with: "-")
        
        database.child(safeEmail).observeSingleEvent(of: .value, with: {snapshot in
            guard snapshot.value as? String != nil else{
                complition(false)
                return
            }
            complition(true)
        })
    }
    
    /// Insert New User to the Real Database
    public func insertUser(with user: ChatAppUser, completion: @escaping(Bool) -> Void){
        database.child(user.safeEmail).setValue([
            "firstName": user.firstName,
            "lastName": user.lastName
            ], withCompletionBlock: { error, _ in
                guard error == nil else{
                    print("failed to write to the  database")
                    completion(false)
                    return
                }
                
                self.database.child("users").observeSingleEvent(of: .value, with: {snapshot in
                    if var usersCollection = snapshot.value as? [[String: String]] { // append to users dictinary
                        let newUserselement = [
                         "name": user.firstName + " " + user.lastName,
                         "email": user.safeEmail
                        ]
                        usersCollection.append(newUserselement)
                        self.database.child("users").setValue(usersCollection, withCompletionBlock: { error, _ in
                            guard error == nil else {
                                completion(false)
                                return
                            }
                            completion(true)
                        })
                        
                    } else { // create new users diictinary
                        let newUsersCollection: [[String: String]] = [
                            ["name": user.firstName + " " + user.lastName,
                             "email": user.safeEmail
                            ]
                        ]
                        self.database.child("users").setValue(newUsersCollection, withCompletionBlock: { error, _ in
                            guard error == nil else {
                                completion(false)
                                return
                            }
                            completion(true)
                        })
                    }
                })
        })
    }
    
    public func getAllUsers(completion: @escaping (Result<[[String: String]], Error>) -> Void){
        database.child("users").observeSingleEvent(of: .value, with: {snapshot in
            guard let value = snapshot.value as? [[String: String]]  else {
                completion(.failure(DatabaseError.failedToFetch))
                return
            }
            completion(.success(value))
        })
    }
    
    public enum DatabaseError: Error{
        case failedToFetch
    }
    
}

// Sending messages + converstions
extension DatabaseMeneger {
    
    // creating new converstion with target user email and sening first message
    public func createNewConverstion(with contactUserEmail: String, name: String, firstMessage: Message, completion: @escaping (Bool) -> Void){
        guard let currentEmail = UserDefaults.standard.value(forKey: "email") as? String else {return}
        let safeEmail = DatabaseMeneger.safeEmail(emailAddress: currentEmail)
        let ref = database.child("\(safeEmail)")
        ref.observeSingleEvent(of: .value, with: { snapshot in
            guard var userNode = snapshot.value as? [String: Any] else {
                completion(false)
                print("user not found")
                return
            }
            
            let messageDate = firstMessage.sentDate
            let dateString = ChatViewController.dateFormatter.string(from: messageDate)
            var message = ""
            
            switch firstMessage.kind {
            case .text(let messageText):
                message = messageText
            case .attributedText(_):
                break
            case .photo(_):
                break
            case .video(_):
                 break
            case .location(_):
                 break
            case .emoji(_):
                 break
            case .audio(_):
                 break
            case .contact(_):
                 break
            case .linkPreview(_):
                 break
            case .custom(_):
                 break
            @unknown default:
                 break
            }
            
            let conversationID = "converstaion_\(firstMessage.messageId)"
            
            let newConverstionData: [String: Any] = [
                "id": conversationID,
                "contact_user_email": contactUserEmail,
                "name": name,
                "latest_message": [
                    "date": dateString,
                    "message_content": message,
                    "is_read": false
                ]
            ]
            
            if var converstions = userNode["conversations"] as? [[String: Any]] {
                //  for the current user the converstion collection is exists, need to append
                converstions.append(newConverstionData)
                ref.setValue(userNode, withCompletionBlock: { [weak self] error, _ in
                    guard error == nil else{
                        completion(false)
                        return
                    }
                    self?.finishCreatingConversation(name: name, conversationID: conversationID, firstMessage: firstMessage, completion: completion)
                })
            }
            else {
                //  for the current user the converstion collection isn't exists, need to create
                userNode["conversations"] = [newConverstionData]
                
                ref.setValue(userNode, withCompletionBlock: { [weak self] error, _ in
                    guard error == nil else{
                        completion(false)
                        return
                    }
                    self?.finishCreatingConversation(name: name, conversationID: conversationID, firstMessage: firstMessage, completion: completion)
                })
            }
        })
    }
    
    private func finishCreatingConversation(name: String, conversationID: String, firstMessage: Message, completion: @escaping (Bool) -> Void){
        
        let messageDate = firstMessage.sentDate
        let dateString = ChatViewController.dateFormatter.string(from: messageDate)
       
        guard let email = UserDefaults.standard.value(forKey: "email") as? String else {
            completion(false)
            return
        }
        let currentUserEmail = DatabaseMeneger.safeEmail(emailAddress: email)
        var message = ""
        
        switch firstMessage.kind {
        case .text(let messageText):
            message = messageText
        case .attributedText(_):
            break
        case .photo(_):
            break
        case .video(_):
             break
        case .location(_):
             break
        case .emoji(_):
             break
        case .audio(_):
             break
        case .contact(_):
             break
        case .linkPreview(_):
             break
        case .custom(_):
             break
        }
        
        let collectionMessage: [String: Any] = [
            "id": firstMessage.messageId,
            "type": firstMessage.kind.MessageKindString,
            "content": message,
            "date": dateString,
            "sender_email": currentUserEmail,
            "isRead": false,
            "name": name
        ]
        
        let value: [String: Any] = [
             "messages": [
                 collectionMessage
             ]
         ]
        
        database.child("\(conversationID)").setValue(value, withCompletionBlock: {error,_ in
            guard error == nil else{
                completion(false)
                return
            }
            completion(true)
        })
    }
    
    // sending messages by given a target converstion
    public func sendMessage (to converstion: String, message: Message, completion: @escaping (Bool) -> Void){}
    
    // return all messages from the database by given a target converstion
    public func getAllMessagesForConverstion(with id: String, completion: @escaping (Result<String, Error>) -> Void){}
    
    // by given email return all converstion from the database for the user
    public func getAllConverstions(for email: String, completion: @escaping (Result<[Conversation], Error>) -> Void){
        database.child("\(email)/conversations").observe(.value, with: { snapshot in
            guard let value = snapshot.value as? [[String: Any]] else {
                completion(.failure(DatabaseError.failedToFetch))
                return
            }
            let converstaions: [Conversation] = value.compactMap({ dictionary in
                guard let conversationId = dictionary["id"] as? String,
                      let name = dictionary["name"] as? String,
                      let contactUserEmail = dictionary["contact_user_email"] as? String,
                      let latestMessage = dictionary["latest_message"] as? [String: Any],
                      let date = latestMessage["date"] as? String,
                      let messageContent = latestMessage["message_content"] as? String,
                    let isRead = latestMessage["is_read"] as? Bool else { return nil}
                
                let latestMessageObject = LatestMessage(Date: date, messageBody: messageContent, isRead: isRead)
                return Conversation(id: conversationId, name: name, contactUserEmail: contactUserEmail, latestMessage: latestMessageObject)
            })
            completion(.success(converstaions))
        })
    }
    
}

struct ChatAppUser {
    let firstName: String
    let lastName: String
    let emailAddress: String
   
    var safeEmail: String {
       let safeEmail = emailAddress.replacingOccurrences(of: ".", with: "-")
       safeEmail.replacingOccurrences(of: "@", with: "-")
      return safeEmail
    }
    
    var profileImageFileName: String {
        return "\(safeEmail)_profile_image.png"
    }
}
