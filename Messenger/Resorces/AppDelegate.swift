//
//  AppDelegate.swift
//  Messenger
//
//  Created by admin on 22/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//


import UIKit
import Firebase
import FBSDKCoreKit
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
       

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

        FirebaseApp.configure()
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        
      GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
      GIDSignIn.sharedInstance()?.delegate = self

        return true
    }

    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {

        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        return GIDSignIn.sharedInstance().handle(url)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error == nil else{
            if let error = error{
                print("Failed to Signin With Google \(error)")
            }
            return
        }
        
        guard let user = user else {
            return
        }
        
        print("Signin with Google \(user)")
        
        // save the google profile information
        guard let email = user.profile.email,
              let firstName = user.profile.givenName,
              let lastName = user.profile.familyName else {
            return
    
        }
        
        UserDefaults.standard.set(email, forKey: "email")
        
        DatabaseMeneger.shared.userExists(with: email, complition: {exists in
            if !exists {
                let userChat = ChatAppUser(firstName: firstName, lastName: lastName, emailAddress: email)
                DatabaseMeneger.shared.insertUser(with: userChat, completion: { success in
                    if success {
                        // upload Image
                        if user.profile.hasImage {
                            guard let url = user.profile.imageURL(withDimension: 200) else {
                                return
                            }
                            
                            URLSession.shared.dataTask(with: url, completionHandler: {data, _, error in
                                guard let data = data else {
                                    return
                                }
                                
                                let fileName = userChat.profileImageFileName
                                StorageManager.shared.uploadProfileImage(with: data, fileName: fileName, completion: {result in
                                    switch result {
                                    case .success(let downloadUrlImage):
                                        UserDefaults.standard.set(downloadUrlImage, forKey: "profile_image_url")
                                        print(downloadUrlImage)
                                    case .failure(let error):
                                        print("Storage Meneger Error: \(error)")
                                    }
                                })
                             }).resume()
                        }
                    }
                })
            }
        })
        
        // take the google token to get a firebase credential
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        
        FirebaseAuth.Auth.auth().signIn(with: credential, completion: { authResult, error in
            guard authResult != nil, error == nil else {
                print("Failed to Login with Google credential")
                return
            }
            
            print("Successfully to Login with Google credential")
            NotificationCenter.default.post(name: .loginNotification, object: nil)
       })
                
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Google user is disconnected")
    }
}

