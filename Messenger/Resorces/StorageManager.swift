//
//  StorageManager.swift
//  Messenger
//
//  Created by admin on 26/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
import FirebaseStorage

final class StorageManager {
    
    static let shared = StorageManager()
    private let storage = Storage.storage().reference()
    
    public typealias UploadPictureCompletion = (Result<String, Error>) -> Void
    
    /// Uploads Profile Image to Firebase Storage and returns completion utl string to download
    public func uploadProfileImage(with data: Data, fileName: String, completion: @escaping UploadPictureCompletion) {
        
        storage.child("images/\(fileName)").putData(data, metadata: nil, completion: { metadata, error in
            guard error == nil else {
                print("Error: upload data to firebase failed!")
                completion(.failure(StorageErrors.failedToUploadImage))
                return
            }
            
            self.storage.child("images/\(fileName)").downloadURL(completion: {url, error in
                guard let url = url else{
                    print("Error: download image url failed!")
                    completion(.failure(StorageErrors.failedToDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print("download image url: \(urlString)")
                completion(.success(urlString))
            })
        })
    }
    
    public enum StorageErrors: Error{
        case failedToUploadImage
        case failedToDownloadUrl
    }
    
    public func downloadImageURL(for path: String, completion: @escaping (Result<URL, Error>) -> Void) {
        let refrence = storage.child(path)
        
        refrence.downloadURL(completion: {url , error in
            guard let url = url, error == nil else{
                completion(.failure(StorageErrors.failedToDownloadUrl))
                return
            }
            
            completion(.success(url))
        })
    }
}
