//
//  ConversationTableViewCell.swift
//  Messenger
//
//  Created by admin on 30/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import SDWebImage

class ConversationTableViewCell: UITableViewCell {
    
    static let identifier = "ConversationTableViewCell"
    
    private let userImageView: UIImageView = {
        let userImageView = UIImageView()
        userImageView.contentMode = .scaleAspectFill
        userImageView.layer.cornerRadius = 50 // make it circle
        userImageView.layer.masksToBounds = true
        
        return userImageView
    }()
    
    private let userNameLabel: UILabel = {
        let userNameLabel = UILabel()
        userNameLabel.font = .systemFont(ofSize: 21, weight: .semibold)
        
        return userNameLabel
    }()
    
    private let userMessageLabel: UILabel = {
        let userMessageLabel = UILabel()
        userMessageLabel.font = .systemFont(ofSize: 19, weight: .regular)
        userMessageLabel.numberOfLines = 0
        return userMessageLabel
    }()
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(userImageView)
        contentView.addSubview(userNameLabel)
        contentView.addSubview(userMessageLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        userImageView.frame = CGRect(x: 10, y: 10, width: 100, height: 100)
        userNameLabel.frame = CGRect(x: userImageView.right + 10, y: 10, width: contentView.width - 20 - userImageView.width, height: (contentView.height - 20) / 2)
        userMessageLabel.frame = CGRect(x: userImageView.right + 10, y: userNameLabel.bottom + 10, width: contentView.width - 20 - userImageView.width, height: (contentView.height - 20) / 2)
    }
    
    public func configure(with model: Conversation){
        self.userMessageLabel.text = model.latestMessage.messageBody
        self.userNameLabel.text = model.name
        
        let profileImagePath = "images/\(model.contactUserEmail)_profile_image.png"
        StorageManager.shared.downloadImageURL(for: profileImagePath, completion: {[weak self] result in
            switch result {
            case .success(let url):
                DispatchQueue.main.async { self?.userImageView.sd_setImage(with: url, completed: nil)}
            case .failure(let error):
                print("Failed to download profile image \(error)")
            }
        })
    }

}
