//
//  ChatViewController.swift
//  Messenger
//
//  Created by admin on 26/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView

struct Message: MessageType{
    public var sender: SenderType
    public var messageId: String
    public var sentDate: Date
    public var kind: MessageKind
}

struct Sender: SenderType {
    public var senderId: String
    public var displayName: String
    public var photoURL: String
}


extension MessageKind {
    var MessageKindString: String{
        switch self {
            
        case .text(_):
            return "text"
        case .attributedText(_):
            return "attributed_text"
        case .photo(_):
            return "photo"
        case .video(_):
            return "video"
        case .location(_):
            return "location"
        case .emoji(_):
            return "emoji"
        case .audio(_):
            return "audio"
        case .contact(_):
            return "contact"
        case .linkPreview(_):
            return "link_preview"
        case .custom(_):
            return "custom"
        }
    }     
}


class ChatViewController: MessagesViewController {
    
    public static let dateFormatter: DateFormatter = {
        let formattre = DateFormatter()
        formattre.dateStyle = .medium
        formattre.timeStyle = .long
        formattre.locale = .current
        return formattre
    }()

    public let contactUserEmail: String
    public var isNewConversation = false
    private var messages = [Message]()
    
    private var selfSender: Sender? {
        guard let email = UserDefaults.standard.value(forKey: "email") as? String else { return nil}
        return Sender(senderId: email, displayName: "Shahar Levi", photoURL: "")
    }
    
   init(with email: String){
        self.contactUserEmail = email
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        view.backgroundColor = .red
                  
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messageInputBar.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        messageInputBar.inputTextView.becomeFirstResponder()
    }
}

extension ChatViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        guard !text.replacingOccurrences(of: " ", with: "").isEmpty,
            let selfSender = self.selfSender,
            let messageID = createMessageID() else {return}
        
        print("Sending: \(text)")
        
        // sending messege logic
        if isNewConversation { // create conversation in database
            let message = Message(sender: selfSender, messageId: messageID, sentDate: Date(), kind: .text(text))
            DatabaseMeneger.shared.createNewConverstion(with: contactUserEmail,name: self.title ?? "user", firstMessage: message, completion: { success in
                if success {
                    print("message sent successfuly")
                }
                else {
                    print("failed to send the massage")
                }
            
            })
        }
        else { // append to existing conversation in database
            
        }
    }
    
    private func createMessageID() -> String? {
        // contactUserEmail + SenderEmail + date
      
        guard let currentUserEmail = UserDefaults.standard.value(forKey: "email") as? String else {return nil}
        
        let safeCurrentUserEmail = DatabaseMeneger.safeEmail(emailAddress: currentUserEmail)
        
        let dateString = Self.dateFormatter.string(from: Date())
        let newIdentifier = "\(contactUserEmail)_\(safeCurrentUserEmail)_\(dateString)"
        print("create messsage id: \(newIdentifier)")
        return newIdentifier
        
    }
}


extension ChatViewController: MessagesDataSource, MessagesLayoutDelegate,MessagesDisplayDelegate {
    func currentSender() -> SenderType {
        if let sender = selfSender {
            return sender
        }
        fatalError("self sender is nil, email should be cached")
        return Sender(senderId: "nil", displayName: "", photoURL: "")
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
}

