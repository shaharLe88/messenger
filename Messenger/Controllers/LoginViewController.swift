//
//  LoginViewController.swift
//  Messenger
//
//  Created by admin on 22/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import FirebaseAuth
import FBSDKLoginKit
import GoogleSignIn
import JGProgressHUD

class LoginViewController: UIViewController {
    
    private let spinner = JGProgressHUD(style: .dark)
    
     private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.clipsToBounds = true
        return scrollView
    }()

    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "messengerLogo")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let emailField: UITextField = {
        let emailField = UITextField()
        emailField.placeholder = "Email Address"
        emailField.autocapitalizationType = .none
        emailField.autocorrectionType = .no
        emailField.returnKeyType = .continue
                
        emailField.layer.cornerRadius = 12
        emailField.layer.borderWidth = 1
        emailField.layer.borderColor = UIColor.lightGray.cgColor
        
        emailField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
        emailField.leftViewMode = .always
        emailField.backgroundColor = .white
       
        return emailField
     }()
    
    private let passwordField: UITextField = {
           let passwordField = UITextField()
           passwordField.placeholder = "Password"
           passwordField.autocapitalizationType = .none
           passwordField.autocorrectionType = .no
           passwordField.returnKeyType = .done
           passwordField.isSecureTextEntry = true
        
           passwordField.layer.cornerRadius = 12
           passwordField.layer.borderWidth = 1
           passwordField.layer.borderColor = UIColor.lightGray.cgColor
        
          passwordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
          passwordField.leftViewMode = .always
          passwordField.backgroundColor = .white
          
           return passwordField
       }()
    
    private let loginButton: UIButton = {
         let loginButton = UIButton()
         loginButton.setTitle("Log In", for: .normal)
         loginButton.setTitleColor(.white, for: .normal)
         loginButton.titleLabel?.font = .systemFont(ofSize: 20, weight: .bold)
         loginButton.backgroundColor = .link
         loginButton.layer.cornerRadius = 12
         loginButton.layer.masksToBounds = true
        
        return loginButton
    }()
    
    private let facebookLoginButton: FBLoginButton = {
           let facebookButton = FBLoginButton()
           facebookButton.permissions = ["email,public_profile"]
           return facebookButton
       }()
    
    private let googleLoginButton = GIDSignInButton()
    
    private var loginObserver: NSObjectProtocol?
    
   override func viewDidLoad() {
        super.viewDidLoad()
    
    loginObserver = NotificationCenter.default.addObserver(forName: Notification.Name.loginNotification, object: nil, queue: .main, using: { [weak self] _ in
        guard let strongSelf = self else {return}
        strongSelf.navigationController?.dismiss(animated: true, completion: nil)
        })
    
       GIDSignIn.sharedInstance()?.presentingViewController = self
    
        title = "Log In"
        view.backgroundColor = .white
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register", style: .done, target: self, action: #selector(registerButtonClicked))
    
        loginButton.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
    
        emailField.delegate = self
        passwordField.delegate = self
        facebookLoginButton.delegate = self
           
        view.addSubview(scrollView)
        scrollView.addSubview(imageView)
        scrollView.addSubview(emailField)
        scrollView.addSubview(passwordField)
        scrollView.addSubview(loginButton)
        scrollView.addSubview(facebookLoginButton)
        scrollView.addSubview(googleLoginButton)
    }
    
    deinit {
        if let observer = loginObserver {
            NotificationCenter.default.removeObserver(loginObserver)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = view.bounds
        let size = scrollView.width / 3
        imageView.frame = CGRect(x: (scrollView.width - size) / 2, y: 20, width: size, height: size)
        emailField.frame = CGRect(x: 30, y: imageView.bottom + 10, width: scrollView.width - 60, height: 52)
        passwordField.frame = CGRect(x: 30, y: emailField.bottom + 10, width: scrollView.width - 60, height: 52)
        loginButton.frame = CGRect(x: 30, y: passwordField.bottom + 10, width: scrollView.width - 60, height: 52)
        facebookLoginButton.frame = CGRect(x: 30, y: loginButton.bottom + 10, width: scrollView.width - 60, height: 52)
        googleLoginButton.frame = CGRect(x: 30, y: facebookLoginButton.bottom + 10, width: scrollView.width - 60, height: 52)
    }
    
    @objc private func registerButtonClicked(){
        let vc = ReigsterViewController()
        vc.title = "Create New Account"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func loginButtonClicked() {
        guard let email = emailField.text, let password = passwordField.text,
            !email.isEmpty, !password.isEmpty, password.count >= 6 else {
              alertUserLoginError()
              return
        }
        
        spinner.show(in: view)
        
        // Firebase Login
        FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password, completion: {[weak self] authResult, error in
           
            guard let strongSelf = self else {return}
            guard let result = authResult, error == nil else {
                print("Faild to Signin with the emailUser: \(email)")
                return
            }
            
            DispatchQueue.main.async {
                strongSelf.spinner.dismiss()
            }
            
            let user = result.user
            UserDefaults.standard.set(email, forKey: "email")
            
            
            print("User Sign in Successfully: \(user)")
            strongSelf.navigationController?.dismiss(animated: true, completion: nil)
        })
    }
    
    func alertUserLoginError(){
        let alertMessege = UIAlertController(title: "Error", message: "The Email otr Password is incorrect", preferredStyle: .alert)
        alertMessege.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alertMessege, animated: true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == emailField){
            passwordField.becomeFirstResponder()
        }
        
        else if textField == passwordField {
            loginButtonClicked()
        }
            
        return true
    }
}

extension LoginViewController: LoginButtonDelegate {
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        // no operation
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        guard let token = result?.token?.tokenString else {
            print("User Failed to Login with Facebook")
            return
        }
        
        // creating facebook request to get email and username
        let facebookRequest = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields":"email, first_name, last_name, picture.type(large)"],
            tokenString: token, version: nil, httpMethod: .get)
        
        // check if the request succedded
        facebookRequest.start(completionHandler: {_, result, error in
            guard let result = result as? [String: Any], error == nil else{
                print("Failed to make facebook graph request")
                return
            }
            
            print("\(result)")
            
            // save the facebook profile information (fetch)
            guard let firstName = result["first_name"] as? String,
                  let lastName = result["last_name"] as? String,
                  let email = result["email"] as? String,
                  let image = result["picture"] as? [String: Any],
                  let data = image["data"] as? [String: Any],
                  let imageUrl = data["url"] as? String else{
                    print("Failed to get email and name from facebook login")
                    return
            }
            
            UserDefaults.standard.set(email, forKey: "email")
         
            // check if the user already exists if not insert the user to database
            DatabaseMeneger.shared.userExists(with: email, complition: {exists in
                if !exists {
                    let userChat = ChatAppUser(firstName: firstName, lastName: lastName, emailAddress: email)
                    DatabaseMeneger.shared.insertUser(with: userChat, completion: {success in
                        if success {
                            // upload image to database
                            
                            guard let url = URL(string: imageUrl) else {
                                return
                            }
                            
                            URLSession.shared.dataTask(with: url, completionHandler:{data, _, _ in
                                guard let data = data else{
                                    return
                                }
                                let fileName = userChat.profileImageFileName
                                StorageManager.shared.uploadProfileImage(with: data, fileName: fileName, completion: {result in
                                    switch result{
                                    case .success(let downloadImageUrl):
                                        UserDefaults.standard.set(downloadImageUrl, forKey: "profile_image_url")
                                        print(downloadImageUrl)
                                    case .failure(let error):
                                        print("Storage Manager Error: \(error)")
                                    }
                                })
                            }).resume()
                            
                        }
                    })
                }
            })
            
            // take the facebook token to get a firebase credential
            let credential = FacebookAuthProvider.credential(withAccessToken: token)
            
            FirebaseAuth.Auth.auth().signIn(with: credential, completion: { [weak self] authRsult, error in
                
                guard let strongSelf = self else {return}
                
                guard authRsult != nil, error == nil else{
                    print("Facebook credential login failed")
                    return
                }
                
                print("Successfully Login With Facebook")
                strongSelf.navigationController?.dismiss(animated: true, completion: nil)
            })
        })
    }
}
