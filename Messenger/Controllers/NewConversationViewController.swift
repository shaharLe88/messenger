//
//  NewConversationViewController.swift
//  Messenger
//
//  Created by admin on 26/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import JGProgressHUD

class NewConversationViewController: UIViewController {
    
    public var completion: (([String: String]) -> (Void))?
    private let spinner = JGProgressHUD(style: .dark)
    private var users = [[String: String]]()
    private var queryResults = [[String: String]]()
    private var firebaseResult = false

    private let searchBar: UISearchBar = {
          let searchBar = UISearchBar()
          searchBar.placeholder = "Search Users"
          return searchBar
    }()
    
    private let tableView: UITableView = {
        let tableview = UITableView()
        tableview.isHidden = true
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tableview
    }()
    
    private let noResultLabel: UILabel = {
        let resultLabel = UILabel()
        resultLabel.isHidden = true
        resultLabel.text = "No Results Found"
        resultLabel.textAlignment = .center
        resultLabel.textColor = .green
        resultLabel.font = .systemFont(ofSize: 21, weight: .medium)
       return resultLabel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        view.backgroundColor = .white
        view.addSubview(tableView)
        tableView.addSubview(noResultLabel)
        tableView.delegate = self
        tableView.dataSource = self
        
        searchBar.delegate = self
        
        navigationController?.navigationBar.topItem?.titleView = searchBar
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissSelf))
        
        searchBar.becomeFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
        noResultLabel.frame  = CGRect(x: view.width / 4, y: (view.height - 200) / 2 , width: view.width / 2, height: 200)
    }
 
    @objc private func dismissSelf(){
        dismiss(animated: true, completion: nil)
    }
}

extension NewConversationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return queryResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = queryResults[indexPath.row]["name"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // start conversion
        let targetUserData = queryResults[indexPath.row]
        dismiss(animated: true, completion: { [weak self] in
            self?.completion?(targetUserData)
        })
        
    }
}


extension NewConversationViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.isEmpty, !text.replacingOccurrences(of: " ", with: "").isEmpty else {
            return
        }
        
        searchBar.resignFirstResponder()
        
        queryResults.removeAll()
        spinner.show(in: view)
        self.searchUsers(query: text)
    }
    
    /* check if the users collection has firebase result
       if it does: we are going to filter this users
       if not, fetch then filter
       update UI: show the result eather there is results or not
     */
    
    func searchUsers(query: String){
        // check if the users collection has firebase result
        
        if firebaseResult {
            // firebase return result from the users collection
            filterUsers(with: query)
        }
        
        else{
            // firebase dosn't return any result, fetch and then filter
            DatabaseMeneger.shared.getAllUsers(completion: { [weak self] result in
                switch result {
                case .success(let usersCollection):
                    self?.firebaseResult = true
                    self?.users = usersCollection
                    self?.filterUsers(with: query)
                case .failure(let error):
                    print("Failed to get users from the collection: \(error)")
                }
            })
        }
    }
    
    // update UI: show the result eather there is results or not
    func filterUsers(with term: String) {
       guard firebaseResult else {return}
        
        self.spinner.dismiss()
       
        let results: [[String: String]] = self.users.filter({
            guard let name = $0["name"]?.lowercased() else {return false}
            return name.hasPrefix(term.lowercased())
        })
        self.queryResults = results
        updateUI()
    }
    
    func updateUI(){
        if queryResults.isEmpty{
            self.noResultLabel.isHidden = false
            self.tableView.isHidden = true
        } else {
            self.noResultLabel.isHidden = true
            self.tableView.isHidden = false
            self.tableView.reloadData() // for refreshing the table view data
        }
    }
}
