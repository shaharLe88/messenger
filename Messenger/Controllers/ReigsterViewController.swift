//
//  ReigsterViewController.swift
//  Messenger
//
//  Created by admin on 22/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import FirebaseAuth
import JGProgressHUD

class ReigsterViewController: UIViewController {
    
 private let spinner = JGProgressHUD(style: .dark)

 private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.clipsToBounds = true
        return scrollView
    }()

    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "person.circle")
        imageView.tintColor = .white
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 2
        imageView.layer.backgroundColor = UIColor.black.cgColor
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let firstNameField: UITextField = {
          let firstNameField = UITextField()
          firstNameField.placeholder = "First Name"
          firstNameField.autocapitalizationType = .none
          firstNameField.autocorrectionType = .no
          firstNameField.returnKeyType = .continue
                  
          firstNameField.layer.cornerRadius = 12
          firstNameField.layer.borderWidth = 1
          firstNameField.layer.borderColor = UIColor.lightGray.cgColor
          
          firstNameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
          firstNameField.leftViewMode = .always
          firstNameField.backgroundColor = .white
         
          return firstNameField
       }()
    
    private let lastNameField: UITextField = {
       let lastNameField = UITextField()
       lastNameField.placeholder = "Last Name"
       lastNameField.autocapitalizationType = .none
       lastNameField.autocorrectionType = .no
       lastNameField.returnKeyType = .continue
               
       lastNameField.layer.cornerRadius = 12
       lastNameField.layer.borderWidth = 1
       lastNameField.layer.borderColor = UIColor.lightGray.cgColor
       
       lastNameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
       lastNameField.leftViewMode = .always
       lastNameField.backgroundColor = .white
      
       return lastNameField
    }()

    private let emailField: UITextField = {
        let emailField = UITextField()
        emailField.placeholder = "Email Address"
        emailField.autocapitalizationType = .none
        emailField.autocorrectionType = .no
        emailField.returnKeyType = .continue
                
        emailField.layer.cornerRadius = 12
        emailField.layer.borderWidth = 1
        emailField.layer.borderColor = UIColor.lightGray.cgColor
        
        emailField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
        emailField.leftViewMode = .always
        emailField.backgroundColor = .white
       
        return emailField
     }()
    
    private let passwordField: UITextField = {
           let passwordField = UITextField()
           passwordField.placeholder = "Password"
           passwordField.autocapitalizationType = .none
           passwordField.autocorrectionType = .no
           passwordField.returnKeyType = .done
           passwordField.isSecureTextEntry = true
        
           passwordField.layer.cornerRadius = 12
           passwordField.layer.borderWidth = 1
           passwordField.layer.borderColor = UIColor.lightGray.cgColor
        
          passwordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
          passwordField.leftViewMode = .always
          passwordField.backgroundColor = .white
          
           return passwordField
       }()
    
    private let registerButton: UIButton = {
         let registerButton = UIButton()
         registerButton.setTitle("Register", for: .normal)
         registerButton.setTitleColor(.white, for: .normal)
         registerButton.titleLabel?.font = .systemFont(ofSize: 20, weight: .bold)
         registerButton.backgroundColor = .systemGreen
         registerButton.layer.cornerRadius = 12
         registerButton.layer.masksToBounds = true
        
        return registerButton
    }()
    
   override func viewDidLoad() {
        super.viewDidLoad()
        title = "Log In"
        view.backgroundColor = .white
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register", style: .done, target: self, action: #selector(registerNavClicked))
    
        registerButton.addTarget(self, action: #selector(registerButtonClicked), for: .touchUpInside)
    
    
        emailField.delegate = self
        passwordField.delegate = self
        
        
        view.addSubview(scrollView)
        scrollView.addSubview(imageView)
        scrollView.addSubview(firstNameField)
        scrollView.addSubview(lastNameField)
        scrollView.addSubview(emailField)
        scrollView.addSubview(passwordField)
        scrollView.addSubview(registerButton)
    
        imageView.isUserInteractionEnabled = true
        scrollView.isUserInteractionEnabled = true
    
        let gesture = UITapGestureRecognizer(target: self, action: #selector(changeProfileImageClicked))
        imageView.addGestureRecognizer(gesture)
    }
    
    @objc private func changeProfileImageClicked() {
       presentPhotoActionSheet()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = view.bounds
        let size = scrollView.width / 3
        imageView.frame = CGRect(x: (scrollView.width - size) / 2, y: 20, width: size, height: size)
        imageView.layer.cornerRadius = imageView.width / 2.0
        firstNameField.frame = CGRect(x: 30, y: imageView.bottom + 10, width: scrollView.width - 60, height: 52)
        lastNameField.frame = CGRect(x: 30, y: firstNameField.bottom + 10, width: scrollView.width - 60, height: 52)
        emailField.frame = CGRect(x: 30, y: lastNameField.bottom + 10, width: scrollView.width - 60, height: 52)
        passwordField.frame = CGRect(x: 30, y: emailField.bottom + 10, width: scrollView.width - 60, height: 52)
        registerButton.frame = CGRect(x: 30, y: passwordField.bottom + 10, width: scrollView.width - 60, height: 52)
        
    }
    
    @objc private func registerNavClicked(){
        let vc = ReigsterViewController()
        vc.title = "Create New Account"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func registerButtonClicked() {

        firstNameField.resignFirstResponder()
        lastNameField.resignFirstResponder()
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
                
        guard let firstName = firstNameField.text,
            let lastName = lastNameField.text,
            let email = emailField.text,
            let password = passwordField.text,
            !firstName.isEmpty, !lastName.isEmpty, !email.isEmpty, !password.isEmpty, password.count >= 6 else {
            alertUserRegisterError()
            return
        }
        
        spinner.show(in: view)
        
        // checking if the user already exsist
        DatabaseMeneger.shared.userExists(with: email, complition: { [weak self] exists in
             guard let strongSelf = self else {return}
             guard !exists else { // user already exists
              strongSelf.alertUserRegisterError()
              return
           }
            
        DispatchQueue.main.async {
             strongSelf.spinner.dismiss()
        }
       
        
        FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password, completion: { authResult, error in
                       
            guard authResult != nil, error == nil else {
                print("Error in creating user")
                return
         }
           
         // insert new user to the firebase
         let userChat = ChatAppUser(firstName: firstName, lastName: lastName, emailAddress: email)
         DatabaseMeneger.shared.insertUser(with: userChat, completion: { success in
            if success {
                guard let image = strongSelf.imageView.image,
                      let data = image.pngData() else {return}
                       
                let fileName = userChat.profileImageFileName
                StorageManager.shared.uploadProfileImage(with: data, fileName: fileName, completion: { result in
                    switch result{
                        case .success(let downloadImageUrl):
                            UserDefaults.standard.set(downloadImageUrl, forKey: "profile_image_url")
                            print(downloadImageUrl)
                        case .failure(let error): print("Storage maneger error: \(error)")
                    }
                 })
             }
         })
            
         strongSelf.navigationController?.dismiss(animated: true, completion: nil)
     })
  })
}
    
    func alertUserRegisterError(){
        let alertMessege = UIAlertController(title: "Error", message: "Please complete the registration", preferredStyle: .alert)
        alertMessege.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alertMessege, animated: true)
    }
}

extension ReigsterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == emailField){
            passwordField.becomeFirstResponder()
        }
        
        else if textField == passwordField {
            registerButtonClicked()
        }
            
        return true
    }
}

extension ReigsterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentPhotoActionSheet(){
        let actionSheet = UIAlertController(title: "Profile Image", message: "Choose the option to Select your Profile Image", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { [weak self] _ in self?.presentCamera() }))
        actionSheet.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { [weak self] _ in self?.presentPhotoPicker() }))
                        
        present(actionSheet, animated: true)
    }
    
    func presentCamera(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    func presentPhotoPicker(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    // for taking photo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
         picker.dismiss(animated: true, completion: nil)
        
        guard let selectedImageProfile = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        
        self.imageView.image = selectedImageProfile
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
